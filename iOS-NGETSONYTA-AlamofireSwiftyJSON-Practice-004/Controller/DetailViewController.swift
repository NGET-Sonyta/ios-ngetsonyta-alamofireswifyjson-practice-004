
import UIKit
import Alamofire

class DetailViewController: UIViewController {
    
    
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var lblCreate: UILabel!
    @IBOutlet weak var lblUpdate: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblID: UILabel!
    
    var myID : String = ""
    var myName : String = ""
    var myCreate : String = ""
    var myUpdate : String = ""
    
    
    var idtest : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        btnDelete.layer.cornerRadius = 15
               
          lblID.text = idtest
          lblName.text = myName
          lblCreate.text = myCreate
          lblUpdate.text = myUpdate
    
    }
    @IBAction func btnDelete(_ sender: Any) {
        // Create alert
        let alert = UIAlertController(title: nil, message: "Are you sure to delete this record?", preferredStyle: UIAlertController.Style.actionSheet)
        
   
        // Configure button handler
        alert.addAction(UIAlertAction(title: "Delete", style: .destructive,
                handler: {(_: UIAlertAction!) in
                    
            // Delete Data from API
               let urlDelete = "http://110.74.194.124:3000/api/teachers/\(self.idtest)"
                    AF.request(urlDelete, method: .delete).response{ response in
                        print(response)
                    }
                    
                    let homeView = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                    
                    // Open View Controller
                    homeView.modalPresentationStyle = .fullScreen
                    self.present(homeView, animated: true, completion: nil)

                }))
        
        // Configure button handler
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
                    
                }))
                self.present(alert, animated: true, completion: nil)
    }
}

extension DetailViewController : DelegateData{
    
    func showData(id: String, name: String, create: String, update: String) {
        idtest  = id
         myName = name
         myCreate = create
         myUpdate = update
    }
}
