import UIKit
import SwiftyJSON
import Alamofire

// Create protocol to delegate
protocol DelegateData {
    
    // Pass data to another Controller via parameter
    func showData(id: String, name: String, create: String, update: String)
}

class ViewController: UIViewController{
   
    @IBOutlet weak var tableView: UITableView!
 
    // Create empty array to store data from API
    var teacher: [Teacher] = []
    
    var dataID : String = ""
    var dataName : String = ""
    var dataCreate : String = ""
    var dataUpdate : String = ""
    
    
    // Create variable of delegate
    var delegateTest: DelegateData?
   

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Hide verticle scroll bar
        tableView.showsVerticalScrollIndicator = false

        tableView.delegate = self
        tableView.dataSource = self
        
        // Register Nib
        tableView.register(TableViewCell.nib(), forCellReuseIdentifier: TableViewCell.identifier)
        
        // Get data from API in JSON format
        AF.request("http://110.74.194.124:3000/api/teachers").responseData { response in
            
            switch response.result {
            
            case .success(let value):
                
                let json = JSON(value)
                
                // Loop JSON data
                    for (key, subJson):(String, JSON) in json[] {
                        
                        //Append data into Teacher struct
                        self.teacher.append(Teacher(json: subJson))
                    }
                self.tableView.reloadData()

            case .failure(_):
                print("Fetch Failed")
            }
        }
    }
    
    func pushScreen() {
        let testView = storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        
        delegateTest = testView as DelegateData
        
        // Pass data via parameter to another controller
        delegateTest?.showData(id: dataID, name: dataName, create: dataCreate, update: dataUpdate )

        testView.modalPresentationStyle = .fullScreen
        
        present(testView, animated: true, completion: nil)
    }
    @IBAction func btnAdd(_ sender: Any) {

        let alert = UIAlertController(title: "Add New Teacher", message: "Please insert a name", preferredStyle: .alert)
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        
        let saveAction = UIAlertAction(title: "Save", style: .default) {_ in
        
        guard let textField = alert.textFields?.first,
              let nameToSave = textField.text else {
                     return
            
        }
  
        
        let urlString = "http://110.74.194.124:3000/api/teachers"
        
        AF.request(urlString, method: .post, parameters:["name": nameToSave], encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            
            switch response.result {
                case .success:
                    print(response)

                    break
                case .failure(let error):

                    print(error)
            }
        }
       
}
        alert.addTextField()
        alert.addAction(cancelAction)
        alert.addAction(saveAction)

        present(alert, animated: true)

        self.tableView.reloadData()

    }
}

extension ViewController : UITableViewDelegate, UITableViewDataSource{
    
     // Create Context Menu
     func tableView(_ tableView: UITableView, contextMenuConfigurationForRowAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
        
        // Pass the indexPath as the identifier for the menu configuration
        return UIContextMenuConfiguration(identifier: indexPath as NSIndexPath, previewProvider: nil) { _ in
            
            // Configure Action in Context Menu
            let shareAction = UIAction(title: "View", image: UIImage(systemName: "square.and.arrow.up")) { _ in
            
                let id = self.teacher[indexPath.row].id
                let name = self.teacher[indexPath.row].name
                let create = self.teacher[indexPath.row].createdAt
                let update = self.teacher[indexPath.row].updatedAt
                
                let testView = self.storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
                
                self.delegateTest = testView as DelegateData
                
                // Pass data via parameter to another controller
                self.delegateTest?.showData(id: id, name: name, create: create, update: update )

                testView.modalPresentationStyle = .fullScreen
                
                self.present(testView, animated: true, completion: nil)
            }
            
            // Configure Action in Context Menu
            let copy = UIAction(title: "Search", image: UIImage(systemName: "doc.on.doc")) { _ in
            print("Search")
                
                
                // Get data from API in JSON format
                AF.request("http://110.74.194.124:3000/api/teachers/5fced8ce918742fe981c388f").responseData { response in

                    switch response.result {

                    case .success(let value):

                        let json = JSON(value)

                        // Loop JSON data
                            for (key, subJson):(String, JSON) in json[] {

                                print(subJson)
                            }
                        self.tableView.reloadData()

                    case .failure(_):
                        print("Fetch Failed")
                    }
                }
                
            }
            
            // Configure Action in Context Menu
            let saveToPhotos = UIAction(title: "Add To Photos", image: UIImage(systemName: "photo")) { _ in
            print("Save to Photos")
            }
            
            // Menu for demonstration purposes
            return UIMenu(title: "Choose one", children: [shareAction, copy, saveToPhotos])
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return teacher.count
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
         dataID = teacher[indexPath.row].id
         dataName = teacher[indexPath.row].name
         dataCreate = teacher[indexPath.row].createdAt
         dataUpdate = teacher[indexPath.row].updatedAt
        
         pushScreen()

    }
    
    // Display Data in Cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCell.identifier) as! TableViewCell
        
        //Show ID and Name in Table Cell
        cell.idLabel.text = teacher[indexPath.row].id
        cell.nameLabel.text = teacher[indexPath.row].name
        return cell
        }
    
    // To Edit Row in Table View
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
            if editingStyle == .delete {

                // Delete Data from API
                let urlDelete = "http://110.74.194.124:3000/api/teachers/\(self.teacher[indexPath.row].id)"
                AF.request(urlDelete, method: .delete).response{ response in
                    print(response)
                }
                
                teacher.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
}


