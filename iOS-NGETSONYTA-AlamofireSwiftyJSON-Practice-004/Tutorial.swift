//
//  Tutorial.swift
//  iOS-NGETSONYTA-AlamofireSwiftyJSON-Practice-004
//
//  Created by Nyta on 12/7/20.
//

import Foundation
import SwiftyJSON

struct Tutorial {
    var title: String
    var description: String
    var teacher: Teacher
    
    init(json: JSON) {
        title = json["title"].stringValue
        description = json["description"].stringValue
        teacher = Teacher(json: json["teacher"])
    }
}
