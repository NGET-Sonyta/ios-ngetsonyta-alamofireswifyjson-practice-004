
import Foundation
import SwiftyJSON

struct Teacher: Codable {
    var id: String
    var name: String
    var createdAt: String
    var updatedAt: String
    
    init(json: JSON) {
        self.id = json["_id"].stringValue
        self.name = json["name"].stringValue
        self.createdAt = json["createdAt"].stringValue
        self.updatedAt = json["updatedAt"].stringValue
    }
}
