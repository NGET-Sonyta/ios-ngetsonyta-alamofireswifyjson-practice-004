//
//  TableViewCell.swift
//  iOS-NGETSONYTA-AlamofireSwiftyJSON-Practice-004
//
//  Created by Nyta on 12/7/20.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    //Create static identifier
    static var identifier = "cardCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    
    }
    
    //Create static function of UINib
    static func nib() -> UINib {
        return UINib(nibName: "TableViewCell", bundle: nil)
    }
    
    //Create func to pass data from UINib to View Controller
    func configureStatus(id: String, name: String){
        self.idLabel?.text = id
        self.nameLabel?.text = name
    }
    
}
